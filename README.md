## Getting Started ##
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Pre-Requirements ###
**Debian, Ubuntu, Linux Mint and Elementary OS**
```bash
sudo apt-get install git build-essential gcc
```

**Fedora, CentOS and Red Hat Linux**
```bash
sudo yum install git make automake gcc gcc-c++ kernel-devel
```

**Arch and Manjaro**
```bash
sudo pacman -S base-devel
```

To verify that GCC has been installed successfully, enter the following command
```bash
gcc --version
```

### Cloning This Repository ###
1. Open Terminal.
2. Change the current working directory to the location where you want the cloned directory to be made.
3. Type `git clone https://gitlab.com/JaganGanesh/C.git`
4. Press Enter. *Your local clone will be created*.

### How to Compile and Run a C Program ###
Use the following command to compile and execute the program.
```bash
gcc filename.c -o filename
./filename
```

### License ###
>This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

>This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0-standalone.html) for more details.

### Contributing Code ###
_We'd love to accept your patches_! If you have improvements, send us your pull requests!
