/*
        Caesar Cipher in C
        Based on the work of Ashutosh Kumar on GeeksForGeeks
        https://www.geeksforgeeks.org/caesar-cipher
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
int main() {
        char text[50]="", encrypt[50]="";
        int shift, length;
        // Get String and Shift Value
        printf("String: ");
        scanf("%s", text);
        printf("Shift Value: ");
        scanf("%d", &shift);
        // Count String Length
        length = strlen(text);
        for(int i=0; i<length; i++) {
                // Uppercase
                if(isupper(text[i]))
                        encrypt[i] = (text[i]+shift-65)%26 +65;
                // Lowercase
                else
                        encrypt[i] = (text[i]+shift-97)%26 +97;
        }
        printf("String Encryption: %s\n", encrypt);
        return 0;
}
